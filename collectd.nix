{ pkgs ? import <nixpkgs> {},
  ...
}:

with pkgs;

let
  collectdMakefileAm =
    stdenv.mkDerivation {
      name = "collectd-Makefile.am-BUILD_PLUGINs";
      src = pkgs.collectd.src;
      phases = [ "unpackPhase" "installPhase" ];
      installPhase = "grep BUILD_PLUGIN Makefile.am > $out";
    };
  plugins =
    let
      findPlugins = s:
        let
          m = builtins.match ".*BUILD_PLUGIN_([A-Z0-9_]+)([.$\n]*)" s;
          plugin = lib.toLower (builtins.elemAt m 0);
          rest = builtins.elemAt m 1;
        in if m == null
           then []
           else [plugin] ++ (findPlugins rest);
    in
      builtins.concatMap findPlugins (
        lib.splitString "\n" (
          builtins.readFile collectdMakefileAm
        )
      );
in
builtins.listToAttrs (
  map (plugin: {
    name = "collectd-plugin-${plugin}";
    value = lib.hydraJob (pkgs.collectd.override {
      enabledPlugins = [ plugin ];
    });
  }) plugins
)
