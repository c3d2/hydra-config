{
  description = "C3D2 Hydra projects";

  outputs = { self, nixpkgs }: {
    packages.x86_64-linux = {
      jobsets =
        nixpkgs.legacyPackages.x86_64-linux.writeText "jobsets.json" (builtins.toJSON ({
          jobsets = {
            type = 1;
            enabled = 1;
            hidden = false;
            checkinterval = 300;
            schedulingshares = 1;
            enableemail = true;
            emailoverride = "";
            keepnr = 12;
            flake = "git+https://gitea.c3d2.de/C3D2/hydra-config.git";
          };
            #inherit (import ./jobsets.nix { pkgs = nixpkgs.legacyPackages.x86_64-linux; }) jobsets;
        }));
    };

    checks = {};
  };
}
