{ pkgs ? import <nixpkgs> { config.allowBroken = true; },
}:

with pkgs;
let
  # TODO: make these work in restricted mode
  blacklist = {
    mucbot = true;
    spaceapi = true;
    storage-ng = true; # missing secrets submodule
    server7 = true;
  };
  physicalHost = host: {
    inherit host;
    configuration = <nix-config> + "/hosts/${host}/configuration.nix";
  };
  containerHost = host: {
    inherit host;
    configuration = <nix-config> + "/hosts/containers/${host}/configuration.nix";
  };
  physicalHosts =
    lib.mapAttrsToList (host: _: physicalHost host) (
      lib.filterAttrs (host: ty: ty == "directory" && host != "containers") (
        builtins.readDir (<nix-config> + "/hosts")
      ));
  containerHosts =
    lib.mapAttrsToList (host: _: containerHost host) (
      lib.filterAttrs (_: ty: ty == "directory") (
        builtins.readDir (<nix-config> + "/hosts/containers")
      ));
  hosts = containerHosts ++ physicalHosts;
  hostSpec = { host, configuration }: rec {
    # pkgs.nixos consumes a NixOS configuration. The toplevel attribute of its return set 
    # contains the switch-to-configuration script that is also usually called by nixos-rebuild
    config = (pkgs.nixos configuration).toplevel;

    # hail expects an activator script in `$out/bin/activate`. We let it run the 
    # switch-to-configuration script with systemd, because if hail is updated
    # itself while switching, it would be killed during the switch
    activator = pkgs.writeScriptBin "activate" ''
        exec -a systemd-run ${pkgs.systemd}/bin/systemd-run \
                --description "Hail: Activate new configuration" \
                ${config}/bin/switch-to-configuration switch
    '';
  };
in
  builtins.listToAttrs (
    builtins.concatMap (spec:
      let
        part = p: {
          name = "${spec.host}-${p}";
          value = lib.hydraJob (hostSpec spec).${p};
        };
      in
      [ (part "config")  (part "activator") ]
    ) (
      builtins.filter ({ host, ... }:
        ! (builtins.hasAttr host blacklist)
      ) hosts
    )
  )   
