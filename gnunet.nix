let
  pkgs = import <nixpkgs> {
    overlays = [ (import <nixpkgs-gnunet>) ];
  };
  gnunet = pkgs.gnunet.overrideAttrs (_: {
    src = <gnunet>;
  });
in
{
  gnunet = pkgs.lib.hydraJob gnunet;
}
