{ pkgs ? import <nixpkgs> {},
  skipTests ? true,
}:
with pkgs;

let
  src = <wikidata-query-rdf>;
  buildInputs = [ jdk maven ];
  mavenFlags = if skipTests
               then "-DskipTests -Dmaven.test.skip=${builtins.toString skipTests}"
               else "";
  buildMavenPackage = subdir: depsHash:
    let
      pomFile = builtins.readFile (src + "/${subdir}/pom.xml");
      name = subdir;
      
      deps = stdenv.mkDerivation {
        name = "${name}-deps";
        inherit src buildInputs;
        buildPhase = ''
            cd ${subdir}
            mvn package -Dmaven.repo.local=$out/.m2 -Dmaven.wagon.rto=5000 ${mavenFlags}
        '';
        installPhase = ''
          find $out/.m2 -type f -regex '.+\(\.lastUpdated\|resolver-status\.properties\|_remote\.repositories\)' -delete
        '';
        outputHashAlgo = "sha256";
        outputHashMode = "recursive";
        outputHash = depsHash;
      };
    in stdenv.mkDerivation {
      name = "${name}";
      inherit src buildInputs;

      buildPhase = ''
        cd ${subdir}
        # 'maven.repo.local' must be writable so copy it out of nix store
        mvn package --offline \
            -Dmaven.repo.local=$(cp -dpR ${deps}/.m2 ./ && chmod +w -R .m2 && pwd)/.m2 \
            ${mavenFlags}
      '';

      installPhase = ''
        mkdir -p $out
        cp -r target/*.?ar $out/
      '';
    };
in {
  blazegraph = lib.hydraJob (buildMavenPackage "blazegraph" "1dkjkib15285lsri1f88ygqbgfbxvfk9pgdk1y598vllf2p6hpsc");
  war = lib.hydraJob (buildMavenPackage "war" "1xh2v71r675dfshlyi6988qh6iii915khalblvv91m40x5sncaqb");
}
