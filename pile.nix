{ pkgs ? import <nixpkgs> {},
}:

with pkgs;
let
  jobs = {
    batman-status = rustPlatform.buildRustPackage {
      name = "batman-status";
      src = <pile/batman-status>;
      cargoSha256 = "1aq2bk448klr0371prqdjflm834nb2yz3kdc0krhgnmjjnasc3d0";
    };
    ledball = rustPlatform.buildRustPackage {
      name = "ledball";
      src = <pile/ledball>;
      cargoSha256 = "0zyfbf3gph8gqab07fmm5a7x5slapsqn8ck6isp53fsa7ljnagjy";
    };
    ledball-globe = rustPlatform.buildRustPackage {
      name = "ledball-globe";
      src = <pile>;
      sourceRoot = "pile/ledball-globe";
      cargoSha256 = "091ndaxsr95q7d0kwp29yz2n3frmf3szy3h6dfdzzd2mwi7ij0bb";
    };
    ledball-impacts = rustPlatform.buildRustPackage {
      name = "ledball-impacts";
      src = <pile>;
      sourceRoot = "pile/ledball-impacts";
      cargoSha256 = "0zyfbf3gph8gqab07fmm5a7x5slapsqn8ck6isp53fsa7ljnagjy";
    };
    ustripe-pulse = rustPlatform.buildRustPackage {
      name = "ustripe-pulse";
      src = <pile/ustripe-pulse>;
      buildInputs = [ libpulseaudio.dev ];
      cargoSha256 = "1sk00wyfhl1ga00rmbk3zm2hnjjwqg149j0g8djlv1mp9wg1ql40";
    };
    ustripe-sha = rustPlatform.buildRustPackage {
      name = "ustripe-sha";
      src = <pile/ustripe-sha>;
      cargoSha256 = "0m2zvnhl4w7ixlqgb2mxx48qgn6lwsdgc1jxzn7dg5m88hzb5ixp";
    };
    ustripe-twinkling = rustPlatform.buildRustPackage {
      name = "ustripe-twinkling";
      src = <pile/ustripe-twinkling>;
      cargoSha256 = "07s77rjgv42ckgbmlxk5pndxv9856fr9n0n08pp37a10jnfs298b";
    };
    ustriped = stdenv.mkDerivation {
      name = "ustriped";
      src = <pile/ustriped>;
      installPhase = ''
        mkdir -p $out/bin
        cp ustriped $out/bin/
      '';
    };
    # Omitted `ustripe-simplex', a node.js project
    # Omitted `pile-top`, unfinished node.js project
    # TODO: rstripee rust-embeddeed project
    # TODO: build openwrt/nixos for rpi
  };
in builtins.mapAttrs (name: lib.hydraJob) jobs
