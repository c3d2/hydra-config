let
  overlay = self: super: {
    collectd = super.collectd.overrideAttrs (oldAttrs: rec {
      name = "collectd-${version}";
      src = <collectd>;
      version = "unstable";
      patches = [];
      nativeBuildInputs = oldAttrs.nativeBuildInputs ++ (
        with super; [
          bison flex perl
        ]
      );
    });
  };
in
import ./collectd.nix {
  pkgs = (
    import <nixpkgs> {
      overlays = [ overlay ];
    }
  );
}
