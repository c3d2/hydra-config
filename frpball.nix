{ pkgs ? import <nixpkgs> { config.allowBroken = true; },
}:

with pkgs;
let
  source = stdenv.mkDerivation {
    name = "frpball-source";
    src = <frpball>;
    buildInputs = [ cabal2nix ];
    buildPhase = ''
      cabal2nix . > package.nix

      cat > default.nix << EOF
      { pkgs ? import <nixpkgs> {},
      }:

      let
        # NixOS 19.09 ships gl-0.9 which is excluded from GPipe's
        # dependency bounds
        gl = with pkgs; with haskellPackages; mkDerivation {
          pname = "gl";
          version = "0.8.0";
          sha256 = "0f8l1ra05asqjnk97sliqb3wqvr6lic18rfs1f9dm1kw2lw2hkda";
          revision = "3";
          editedCabalFile = "0q8d4237ds78y4p35xl2arlmmpgs2ag7krw9chby6q9dcs00zxrl";
          setupHaskellDepends = [
            base Cabal containers directory filepath hxt transformers
          ];
          libraryHaskellDepends = [
            base containers fixed half transformers
          ];
          librarySystemDepends = [ libGL ];
          description = "Complete OpenGL raw bindings";
          license = stdenv.lib.licenses.bsd3;
        };
        haskellPackages = pkgs.haskellPackages.override {
          overrides = self: super: {
            inherit gl;
          };
        };
      in
      haskellPackages.callPackage ./package.nix {}
      EOF
    '';
    installPhase = "cp -ar . $out";
  };
  ghcVersions =
    builtins.filter (p:
      p == "ghcHEAD" || builtins.match "ghc[[:digit:]]+" p != null
    ) (builtins.attrNames pkgs.haskell.packages);
  frpball = haskellPackages:
    import source {
      pkgs = pkgs // { inherit haskellPackages; };
    };
in
builtins.listToAttrs
  (map (ghcVersion: {
    name = "frpball-${ghcVersion}";
    value = pkgs.lib.hydraJob (frpball (builtins.getAttr ghcVersion haskell.packages));
  }) ghcVersions)
