{ pkgs ? import <nixpkgs> {},
}:

with pkgs;
let
  spaceapi = import <spacemsg/spaceapi> {
    inherit pkgs;
  };
  schalterd = rustPlatform.buildRustPackage {
    name = "schalterd";
    src = <spacemsg/schalterd>;
    cargoSha256 = "1h83w1n8zygap7cjqwjygcr9ndj3jam31pb2jpayswk4fr4a62v3";
  };
  jobs = {
    inherit spaceapi schalterd;
  };
in builtins.mapAttrs (name: lib.hydraJob) jobs
