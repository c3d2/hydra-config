{ pkgs ? import <nixpkgs> {}
}:
let
  ghcVersions =
    builtins.filter (p:
      p == "ghcHEAD" || builtins.match "ghc[[:digit:]]+" p != null
    ) (builtins.attrNames pkgs.haskell.packages);
  mateamt = haskellPackages: haskellPackages.callPackage <mateamt> {};
in
builtins.listToAttrs
  (map (ghcVersion: {
    name = "mateamter-${ghcVersion}";
    value = pkgs.lib.hydraJob (mateamt (builtins.getAttr ghcVersion pkgs.haskell.packages));
  }) ghcVersions)
