{ pkgs ? import <nixpkgs> {} }:
let
  mkJobset = { description, nixexprpath, gitUrls }: {
    inherit description nixexprpath;
    enabled = 1;
    hidden = false;
    nixexprinput = "hydra-config";
    checkinterval = 300;
    schedulingshares = 1;
    enableemail = true;
    emailoverride = "";
    keepnr = 12;
    type = 0;
    inputs = {
      hydra-config = {
        type = "git";
        value = "https://gitea.c3d2.de/C3D2/hydra-config.git master 1";
        emailresponsible = false;
      };
      nixpkgs = {
        type = "git";
        value = "https://github.com/NixOS/nixpkgs.git release-20.03 1";
        emailresponsible = false;
      };
    } // (builtins.mapAttrs (_: gitUrl: {
      type = "git";
      value = gitUrl;
      emailresponsible = false;
    }) gitUrls);
  };
  jobsets = {
    # TODO: host tests
    spacemsg = mkJobset {
      description = "Astro's HQ IoT stuff";
      nixexprpath = "spacemsg.nix";
      gitUrls = {
        spacemsg = "https://github.com/astro/spacemsg.git master 1";
      };
    };

    yammat = mkJobset {
      description = "Matemat by nek0";
      nixexprpath = "yammat.nix";
      gitUrls = {
        yammat = "https://gitea.nek0.eu/nek0/yammat.git master 1";
      };
    };

    mateamt = mkJobset {
      description = "Mateamt by nek0";
      nixexprpath = "mateamt.nix";
      gitUrls = {
        mateamt = "https://github.com/nek0/mateamt.git master 1";
        haskell-nix = "https://github.com/input-output-hk/haskell.nix master 1";
      };
    };

    matebeamter = mkJobset {
      description = "Mateamt frontend by nek0";
      nixexprpath = "matebeamter.nix";
      gitUrls = {
        mateamt = "https://github.com/nek0/mateamt.git master 1";
        matebeamter = "https://github.com/nek0/matebeamter.git master 1";
      };
    };

    tigger = mkJobset {
      description = "XMPP MUC bot";
      nixexprpath = "tigger.nix";
      gitUrls = {
        tigger = "https://github.com/astro/tigger.git master 1";
      };
    };

    slidenado = mkJobset {
      description = "CmS slide generator";
      nixexprpath = "slidenado.nix";
      gitUrls = {
        slidenado = "https://github.com/c3d2/slidenado.git master 1";
      };
    };

    pile = mkJobset {
      description = "Programatically Illuminated Lighting Environment";
      nixexprpath = "pile.nix";
      gitUrls = {
        pile = "https://github.com/astro/pile.git master 1";
      };
    };

    gnunet = mkJobset {
      description = "gnu:net overlay";
      nixexprpath = "gnunet.nix";
      gitUrls = {
        gnunet = "https://git.gnunet.org/gnunet.git master";
        nixpkgs-gnunet = "https://code.ulrich.earth/christian/nixpkgs-gnunet master";
      };
    };

    ticker = mkJobset {
      description = "Calendar aggregation";
      nixexprpath = "ticker.nix";
      gitUrls = {
        ticker = "https://gitea.c3d2.de/astro/ticker.git master 1";
        mozillaOverlay = "https://github.com/mozilla/nixpkgs-mozilla.git master 1";
      };
    };

    frpball = mkJobset {
      description = "Functional reactive programming ball game";
      nixexprpath = "frpball.nix";
      gitUrls = {
        frpball = "https://github.com/nek0/frpball.git master 1";
      };
    };

    dn42-registry = mkJobset {
      description = "dn42 registry checks";
      nixexprpath = "dn42-registry.nix";
      gitUrls = {
        registry = "https://git.dn42.us/dn42/registry.git";
      };
    };

    hail = mkJobset {
      description = "NixOS builds for deployment through Hail";
      nixexprpath = "hail.nix";
      gitUrls = {
        nix-config = "https://gitea.c3d2.de/C3D2/nix-config.git";
        yggdrasil-package-nix = "https://github.com/ehmry/yggdrasil-package-nix.git";
        nixpkgs = "https://github.com/NixOS/nixpkgs.git release-20.03";
        nixpkgs-unstable = "https://github.com/NixOS/nixpkgs.git master";
      };
    };

    wikidata-query-rdf = mkJobset {
      description = "wikidata/query/rdf";
      nixexprpath = "wikidata-query-rdf.nix";
      gitUrls = {
        wikidata-query-rdf = "https://github.com/wikimedia/wikidata-query-rdf.git";
      };
    };

    wikidata-query-rdf-graviola = mkJobset {
      description = "wikidata/query/rdf (gra.one fork)";
      nixexprpath = "wikidata-query-rdf.nix";
      gitUrls = {
        wikidata-query-rdf = "https://git.gra.one/graviola/wikidata/wikidata-query-rdf.git";
      };
    };

    collectd = mkJobset {
      description = "Test dependencies for each collectd plugin";
      nixexprpath = "collectd.nix";
      gitUrls = {
        nixpkgs = "https://github.com/astro/nixpkgs.git collectd-plugins 1";
      };
    };

    collectd-master = mkJobset {
      description = "Test dependencies for each collectd plugin";
      nixexprpath = "collectd-master.nix";
      gitUrls = {
        collectd = "https://git.octo.it/collectd.git";
        nixpkgs = "https://github.com/NixOS/nixpkgs.git nixos-unstable";
      };
    };

    "36c3-everything" = mkJobset {
      description = "All 36C3 events in HTML";
      nixexprpath = "c3-everything.nix";
      gitUrls = {
        html = "https://github.com/c3d2/36c3-everything.git";
        schedule = "https://github.com/voc/36C3_schedule.git";
      };
    };

    aparte = mkJobset {
      description = "Rust console XMPP client";
      nixexprpath = "aparte.nix";
      gitUrls = {
        aparte = "https://github.com/paulfariello/aparte.git";
        mozillaOverlay = "https://github.com/mozilla/nixpkgs-mozilla.git";
      };
    };

    c3d2-web = mkJobset {
      description = "www.c3d2.de";
      nixexprpath = "c3d2-web.nix";
      gitUrls = {
        c3d2-web = "http://git.c3d2.de/c3d2-web.git";
      };
    };
  };

  jobsetsJson = pkgs.writeText "jobsets.json" (builtins.toJSON jobsets );
in
{
  jobsets = pkgs.runCommand "jobsets.json" {} ''
    cp ${jobsetsJson} $out
  '';
}
