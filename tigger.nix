{pkgs ? import <nixpkgs> {}}:

{
  tigger = pkgs.lib.hydraJob (import <tigger> {
    inherit pkgs;
  });
}
