{ pkgs ? import <nixpkgs> {}
}:
with pkgs;
let
  yammat = haskellPackages:
    haskellPackages.callPackage <yammat> {};
  ghcVersions =
    builtins.filter (p:
      p == "ghcHEAD" || builtins.match "ghc[[:digit:]]+" p != null
    ) (builtins.attrNames haskell.packages);
in

builtins.listToAttrs
  (map (ghcVersion: {
    name = "yammat-${ghcVersion}";
    value = lib.hydraJob (yammat (builtins.getAttr ghcVersion haskell.packages));
  }) ghcVersions)
