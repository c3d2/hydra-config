{ pkgs ? import <nixpkgs> {},
  schedule ? <schedule>
}:
with pkgs;
{
  "36c3-everything" = lib.hydraJob ((
    import <html> {
      inherit pkgs schedule;
    }
  ).overrideAttrs (oldAttrs: {
    installPhase = ''
      ${oldAttrs.installPhase}

      mkdir $out/nix-support
      echo doc CSS style.css >> $out/nix-support/hydra-build-products

      for f in $out/?.html ; do
        d=`basename $f|sed -e s/.html//`
        substituteInPlace $f \
          --replace style.css ../1/style.css
        echo doc Day$d $f >> $out/nix-support/hydra-build-products
      done
    '';
  }));
}
