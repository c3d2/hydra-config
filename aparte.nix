{ pkgs ? import <nixpkgs> { overlays = [ (import <mozillaOverlay>) ]; } }:
with pkgs;

let
  rustManifest = ./channel-rust-nightly.toml;
  rustChannel = lib.rustLib.fromManifestFile rustManifest {
    inherit stdenv fetchurl patchelf;
  };
  rust = rustChannel.rust;
  rustPlatform = recurseIntoAttrs (makeRustPlatform {
    rustc = rust;
    cargo = rust;
  });
  aparte = rustPlatform.buildRustPackage {
    name = "aparte";
    src = <aparte>;
      cargoSha256 = "1r93cvbhckz0a933pc2lcjvsw9zv8s8a2z6z5qiy9y4zzrhi7zv5";
    buildInputs = [ pkgconfig openssl ];
  };
in {
  aparte = lib.hydraJob aparte;
}
