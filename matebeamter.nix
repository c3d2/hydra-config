{ pkgs ? import <nixpkgs> { config.allowBroken = true; },
}:

with pkgs;
let
  source = stdenv.mkDerivation {
    name = "matebeamter-source";
    src = <matebeamter>;
    buildInputs = [ cabal2nix ];
    buildPhase = ''
      cabal2nix . > package.nix
    '';
    installPhase = "cp -ar . $out";
  };
  ghcVersions =
    builtins.filter (p:
      p == "ghcHEAD" || builtins.match "ghc[[:digit:]]+" p != null
    ) (builtins.attrNames pkgs.haskell.packages);
  matebeamter = haskellPackages: with haskellPackages;
    let
      mateamt = callPackage <mateamt/shell.nix> {
        nixpkgs = pkgs // { inherit haskellPackages; };
      };
    in
    callPackage "${source}/package.nix" { inherit mateamt; };
in
builtins.listToAttrs
  (map (ghcVersion: {
    name = "matebeamter-${ghcVersion}";
    value = pkgs.lib.hydraJob (matebeamter (builtins.getAttr ghcVersion haskell.packages));
  }) ghcVersions)
