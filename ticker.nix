{ pkgs ? import <nixpkgs> {},
}:

with pkgs;
with import <ticker> {};
{
  ticker-update = lib.hydraJob ticker-update;
  ticker-serve = lib.hydraJob ticker-serve;
}
