{ pkgs ? import <nixpkgs> {} }:
with pkgs;

let
  registry = stdenv.mkDerivation {
    name = "dn42-registry";
    src = <registry>;
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
    nativeBuildInputs = [ python3 ];
    patchPhase = "patchShebangs utils";
    installPhase = "cp -ar . $out";
  };
  check = name: arg: stdenv.mkDerivation {
    inherit name;
    phases = [ "installPhase" "checkPhase" ];
    installPhase = "touch $out";
    buildInputs = [ python3 ];
    doCheck = true;
    checkPhase = ''
      cd ${registry}
      ./check-my-stuff ${arg}
    '';
  };
in {
  all = lib.hydraJob (check "all" "--all");
}
