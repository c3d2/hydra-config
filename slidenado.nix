{pkgs ? import <nixpkgs> {},
}:

with pkgs;

let
  presentations =
    map (match: builtins.elemAt match 0)
    (builtins.filter (match: match != null)
      (map (builtins.match "(.+)\.tex")
        (builtins.attrNames
          (lib.filterAttrs (name: type: type == "regular")
            (builtins.readDir <slidenado/presentations>)
          ))));
  presentation = name:
    stdenv.mkDerivation {
      name = "slidenado-${name}";
      src = <slidenado>;
      nativeBuildInputs = [ texlive.combined.scheme-full perl ];
      HOME = "/tmp";
      makeFlags = [ "presentations/${name}.pdf" ];
      installPhase = ''
        mkdir -p $out/nix-support
        cp presentations/${name}.pdf $out/
        echo doc-pdf slides $out/${name}.pdf >> $out/nix-support/hydra-build-products
      '';
    };
in

builtins.listToAttrs
  (map (name: {
    inherit name;
    value = lib.hydraJob (presentation name);
  }) presentations)
